import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, MultiDataSet, MultiLineLabel} from 'ng2-charts';

@Component({
  selector: 'app-screen1',
  templateUrl: './screen1.component.html',
  styleUrls: ['./screen1.component.css']
})
export class Screen1Component implements OnInit {

  //Pie Chart

  //predefined data for pie chart
  public var1=500;
  public var2=500;
  public var3=700;
  public var4=100;

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ['Food','Travel','Rent', 'Entertainment'];
  public pieChartData: SingleDataSet = [this.var1, this.var2, this.var3, this.var4];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  //Bar Graph Chart 


  public barChartOptions: ChartOptions={
    responsive:true
  };
  public barChartLabels: Label[] = ['Ferrari', 'Lamboghini', 'Jeep']
  public barChartType: ChartType='bar';
  public barChartLegend: true;
  public barChartPlugins =[];

  //predefined data for bar graph chart
  public barChartData: ChartDataSets[]=[
    { data:[380, 400, 390], label:"2018" },
    { data:[390, 390, 450], label:"2019" }
  ];


  //Doughnut Chart

  public doughnutChartLabels: Label[]=['Hp', 'Dell', 'Apple']
  //predefined data for dougnut chart
  public doughnutChartData: MultiDataSet=[
    [100, 100, 100],
    [200, 100, 300],
    [50, 400, 50]
  ];
  public doughnutChartType: ChartType='doughnut';
  public doughnutChartLegend:true;


  constructor() { 
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit() {
  }
  
  //xyz, abc, uvw, uvw1, uvw2, xyz... are just paramater names

  //pushing in pie chart
  submit1(xyz, abc)
  {

    this.pieChartLabels.push(xyz);
    this.pieChartData.push(abc);
  }

  //pushing in bar graph
  submit2(uvw, uvw1, uvw2)
  {
    this.barChartLabels.push(uvw);
    this.barChartData[0].data.push(uvw1);
    this.barChartData[1].data.push(uvw2);

  }

  //pushing in doughnut chart
  submit3(abc, xyz1, xyz2, xyz3)
  {
    this.doughnutChartLabels.push(abc);
    this.doughnutChartData[0].push(xyz1);
    this.doughnutChartData[1].push(xyz2);
    this.doughnutChartData[2].push(xyz3);

  }
}


