import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen3',
  templateUrl: './screen3.component.html',
  styleUrls: ['./screen3.component.css']
})
export class Screen3Component implements OnInit {
  minlength: number = 0 //length of shortest word
  maxlength: number = 0 //length of longest word
  vowels: number=0 //count of vowels
  artcount: number=0 //count of articles
  hasText: Boolean=false //variable to show the results, if true, results will be shown
  a: any[] //a will store split array
  wc: number=0 // word count

  constructor() { }

  ngOnInit() {
  }

  manupulation(abc)
  {

    this.a=abc.split(' '); //splits array on spaces and returns a new array

    //word count
    this.wc=this.a.length

    //For vowel count
    for(let i=0; i<abc.length; i++)
    {
      //logic to check vowels
      if(abc[i]=='a' || abc[i]=='e' || abc[i]=='i' || abc[i]=='o' || abc[i]=='u' ||
      abc[i]=='A' || abc[i]=='E' || abc[i]=='I' || abc[i]=='O' || abc[i]=='U')
      {
        this.vowels++;
      }
    }

    //For Articles count
    for(let i=0; i<this.a.length; i++)
    {
      if(this.a[i]=='a' || this.a[i]=='an' || this.a[i]=='the')
      {

        this.artcount++
      }
    }

    //Initializing lengths of first word (an edge case)
    this.maxlength=this.a[0].length;
    this.minlength=this.a[0].length;

    
    //longest word length
    for(let i=0; i<this.a.length-1; i++)
    {

      if(this.a[i].length> this.a[i+1].length)
      {
        if(this.a[i].length>this.maxlength)
        this.maxlength=this.a[i].length
      }
      else
      {
        if(this.a[i+1].length>this.maxlength)
        this.maxlength=this.a[i+1].length
      }
    }



    //shortest word length
    for(let i=0; i<this.a.length-1; i++)
    {

      if(this.a[i].length< this.a[i+1].length)
      {
        // condition after && is for excluding 'a', 'an' and 'the' from shortest words
        if(this.a[i].length<this.minlength && this.a[i]!='a' && this.a[i]!='an' && this.a[i]!='the')
        this.minlength=this.a[i].length
      }
      else
      {
        // condition after && is for excluding 'a', 'an' and 'the' from shortest words
        if(this.a[i+1].length<this.minlength && this.a[i+1]!='a' && this.a[i]!='an' && this.a[i]!='the')
        this.minlength=this.a[i+1].length
      }
    }

    this.hasText=true //becomes true at the end of submit function and displays the output
  }

}
