import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

//importing data from json
import { data1 } from './data'; 
import { data2 } from './data';

@Component({
  selector: 'app-screen2',
  templateUrl: './screen2.component.html',
  styleUrls: ['./screen2.component.css']
})
export class Screen2Component implements OnInit {

  //abc and def are values of json object data1 and data2 respectively
  abc=Object.values(data1);
  def=Object.values(data2);
  //xyz and uvw are keys of json object data1 and data2 respectively
  xyz=Object.keys(data1);
  uvw=Object.keys(data2);

  //random1 and random2 are random variables in random function
  random1: number=0
  random2: number=0

  //year is last year from json object that is initialiazed to 2018 
  year:string ='2018'

  //x is year converted to number 
  x:number=parseInt(this.year);
  

  //Line Chart Data
  public lineChartData: ChartDataSets[]=
  [
    {
      //json data is pushed in line1 graph
      data:this.abc,
      label: 'Rainfall in mm' 
    },
    {
      //json data is pushed in line2 graph
      data:this.def,
      label:'Rainfall in mm'
    }
  ];
    
  public lineChartLabels: Label[]=this.xyz;
  public lineChartOptions: (ChartOptions)=
  {
    responsive: true,
  };
  public lineChartColors: Color[]=[
    //defines the line properties
    {
      borderWidth:1,
      borderColor:'black',
      backgroundColor:'rgba(0, 0, 0, 0)'
    },
    {
      borderWidth:1,
      borderColor:'blue',
      backgroundColor:'rgba(0,0,0,0)'
    },
    {
      borderWidth:1,
      borderColor:'red',
      backgroundColor:'rgba(0,0,0,0)'
    }
  ];
  public lineChartLegend=true;
  public lineChartType='line';
  public lineChartPlugins=[];
  

  //Area chart
  public areaChartData: ChartDataSets[]=
  [
    {
      //json data is pushed in area1 graph
      data:this.def,
      label: 'Rainfall in mm' 
    },
    {
      //json data is pushed in area2 graph
      data:this.abc,
      label: 'Rainfall in mm'
    }
  ];

  public areaChartLabels: Label[]=this.uvw
  public areaChartOptions: (ChartOptions)=
  {
    responsive: true,
  };
  public areaChartColors: Color[]=[
    //defines properies for area graph
    {
      backgroundColor:'rgba(255, 0, 0, 0.3)'
    },
    {
      backgroundColor:'rgba(255,0,255, 0.3)'
    }
  ];

  public areaChartLegend=true;
  public areaChartType='line';
  public areaChartPlugins=[];
  constructor() { }
  

  //function to generate random values
  random()
  {
    
    this.random1=Math.floor(Math.random()*1000)
    this.random2=Math.floor(Math.random()*1000)
    //data is pushed in line chart
    this.lineChartData[0].data.push(this.random1);
    this.x++;
    let y=this.x.toString()
    //label is pushed in line chart
    this.lineChartLabels.push(y);

    //data is pushed in area chart
    this.areaChartData[0].data.push(this.random2);
    //label is pushed in area chart
    this.areaChartLabels.push(y);
     
  }
  

  ngOnInit() {
    //set interval to update random values
    setInterval(()=>{
      this.random();
    },1000);
    
  }
  
}
